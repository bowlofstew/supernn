/*
    This file is part of SuperNN.

    SuperNN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SuperNN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#ifndef SUPERNN_ACTIVATION_TYPE_HPP
#define SUPERNN_ACTIVATION_TYPE_HPP

namespace SuperNN
{
    /**
     * Activation functions built-in in the library.
     */
    enum ActFuncType
    {
        /** Sigmoid activation function */
        ACT_SIGMOID = 0,

        /** Sigmoid symmetric activation function */
        ACT_SIGMOID_SYMMETRIC,

        /** Linear activation function */
        ACT_LINEAR,

        /** Sigmoid-like activation function */
        ACT_ELLIOT,

        /** Sigmoid-like activation function, symmetric version */
        ACT_ELLIOT_SYMMETRIC,

        /** Gaussian activation function */
        ACT_GAUSSIAN,

        /** Gaussian symmetric function */
        ACT_GAUSSIAN_SYMMETRIC,

        /** Sign */
        ACT_SIGN,

        /** End marker */
        ACT_LAST
    };
}

#endif
