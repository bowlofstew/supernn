/*
    This file is part of SuperNN.

    SuperNN is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SuperNN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SuperNN.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2010 - 2015 Lucas Hermann Negri
*/

#ifndef SUPERNN_DATA_HPP
#define SUPERNN_DATA_HPP

#include <vector>
#include <string>
#include <fstream>
#include "utils.hpp"

namespace SuperNN
{
static const double FSMALL = 1e-9;

/** Minimum / maximum scaling information */
struct SUPERNN_EXPORT SInfo
{
    SInfo();
    ~SInfo();

    SInfo(double _min, double _max);

    double scale(const SInfo &to, double value) const;
    double min, max;
};

/**
 * Data scaling information, for all input and output neurons.
 */
struct SUPERNN_EXPORT Bounds : public std::vector<SInfo>
{
    Bounds();
    virtual ~Bounds();

    /**
     * Merges the values from another Bounds with the current, retaining the
     * limits. Only the caller object is modified. For example, if the bounds
     * { {-10, 10 } ,{5 , 10} } and { { -5, 20} ,{0, 5} } were merged, the
     * result would be { {-10, 20}, {0, 10} }.
     *
     * @param other Bounds object to merge with
     */
    void merge_with(const Bounds &other);

    /**
     * Appends the data bounds values into a file stream.
     *
     * @param out Output file stream
     */
    void save_file(std::ofstream &out) const;

    /**
     * Reads the scaling information from a file stream.
     *
     * @param out Output file stream
     */
    void load_file(std::ifstream &inp);

    /**
     * Loads from and to bounds from a file.
     *
     * @param path Path to the file with both from and to bounds
     * @param from From bounds to be populated
     * @param to   To bounds to be populated
     * @throw Exception if it couldn't open the file
     */
    static void load_file(const std::string &path, Bounds &from, Bounds &to);
};

/**
 * Data row.
 */
typedef std::vector<double> Row;

/**
 * Data used in training, validation and testing.
 */
struct SUPERNN_EXPORT Data : public std::vector<Row>
{
    Data();
    virtual ~Data();

    /**
     * Constructor.
     *
     * @param rows Number of rows
     * @param cols Number of columns (inputs + outputs)
     */
    Data(unsigned rows, unsigned cols);

    /**
     * Returns a copy of the current object, without a column.
     *
     * @param col Column that will be missing
     * @return Copy of the current object, without a column
     */
    Data drop_column(unsigned col) const;

    /**
     * Adds a row to the data, returning a reference to it.
     *
     * @return Reference to the added row
     */
    Row &add();

    /**
     * Constructor. Reads the values from a static array.
     *
     * @param rows Number of rows
     * @param n    Number of columns (inputs + outputs)
     * @param st   Static array with the data values
     */
    Data(unsigned rows, unsigned cols, const double st[]);

    /**
     * Constructor. Reads the values from a vector.
     *
     * @param rows Number of rows
     * @param n    Number of columns (inputs + outputs)
     * @param st   Vector (one dimension) with the data values
     */
    Data(unsigned rows, unsigned cols, const Row &row);

    /**
     * Randomizes the positions of the rows.
     */
    void shuffle();

    /**
     * Samples the data.
     *
     * @param first First row (zero-based, including)
     * @param last  Last row (zero-based, excluding)
     * @return A new data object containing the rows from first to last
     */
    Data sample(unsigned first, unsigned last) const;

    /**
     * Reads values from a file and appends then into the data.
     *
     * @param path Input file path
     * @return Number of added rows
     * @throw Exception if it couldn't open the file or it has invalid contents
     */
    unsigned load_file(const std::string &path);

    /**
     * Erases the contents of a file and saves the Data values into it.
     *
     * @param path Output file path
     * @throw Exception if the output file couldn't be written
     */
    void save_file(const std::string &path) const;

    /**
     * Erases the contents of a file and saves the Data bounds info into it.
     *
     * @param path Output file path
     * @throw Exception if the output file couldn't be written
     */
    void save_info_file(const std::string &path) const;

    /**
     * Calculates and sets the data bounds using the minimum and maximum
     * values of each neuron.
     *
     * @return Calculated bounds
     */
    Bounds &calc_bounds();

    /**
     * Scales a single data column.
     *
     * @param n    Column to scale
     * @param curv Current bounds
     * @param newv New bounds
     */
    void scale_column(unsigned n, const SInfo &curv, const SInfo &newv);

    /**
     * Scales the data, neuron per neuron, using the current from and to
     * bounds.
     * You need to set the bounds prior to calling this function.
     */
    void scale();

    /**
     * Scales the data, neuron per neuron, using the current "from" bounds.
     * Sets the "to" bounds of every neuron to min and max.
     * You need to call calc_bounds() or manually set the from bounds prior
     * to calling this function.
     *
     * @param min New minimum value
     * @param max New maximum value
     */
    void scale(double min, double max);

    /**
     * Descales the data.
     */
    void descale();

    /**
     * Fills two Data objects with complementary information, useful for
     * cross-validation. The first Data object is filled with the N-th partition (0 based)
     * of the original data, while the another one is filled with the remaining rows.
     * Each one of the K partitions have roughly the same size, and the original
     * data is left unmodified.
     *
     * @param n Partition to get
     * @param k Total number of partitions
     * @param p Data that will be filled with the rows from the selected partition
     * @param l Data that will be filled with the remaining rows
     */
    void k_fold(unsigned n, unsigned k, Data &p, Data &l) const;

    /**
     * Returns the value in the (row,col) cell. Main a helper for bindings to other
     * programming languages.
     *
     * @param row Row
     * @param col Column
     * @return Value at (row,col)
     */
    inline double get(unsigned row, unsigned col) const
    {
        return at(row)[col];
    }

    /**
     * Sets the value in the (row,col) cell to value. Main a helper for bindings to other
     * programming languages.
     *
     * @param row Row
     * @param col Column
     * @param value Value to write at (row,col)
     */
    inline void set(unsigned row, unsigned col, double val)
    {
        at(row)[col] = val;
    }

    /** Original data bounds */
    Bounds from;

    /** Scaled data bounds */
    Bounds to;

    /** Number of inputs + outputs per row */
    size_t n_total;
};
}

#endif
