#include <supernn>
#include <iostream>
#include "config.hpp"

using namespace SuperNN;
using namespace std;

int main(int argc, char* argv[])
{
    Utils::rand_seed();
    
    /* just a line fit */
    Network net = Network::make_mlp(1, 1, 1);
    net.set_activation(ACT_LINEAR);
    net.init_weights(-0.5, 0.5);
    
    /* training algorithm */
    TrainingAlgorithm* t; /* std::unique_ptr if we were using C++11 */
    
    if(argc == 2 && string(argv[1]) == string("L1"))
        t = new IRpropL1();
    else
        t = new IRprop();
    
    t->prepare(net);
    
    Data full;
    full.load_file(TEST_DIR + "approx/approx.full");
    
    /* training */
    const unsigned e = t->train(net, full, 1e-3, 100);
    const double mse = net.calc_mse(full);
    const double mae = net.calc_mae(full);

    cerr << "Epochs : " << e << endl;
    cerr << "MSE    : " << mse << " MAE: " << mae << endl;
    
    /* output the approximation */
    for(double x = 0; x < 1; x += 0.001)
    {
        Row inp(1);
        inp[0] = x;
        cout << x << " " << net.run(inp)[0] << endl;
    }
    
    delete t;
    
    return 0;
}
