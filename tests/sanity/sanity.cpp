#include <supernn>
#include <cassert>
#include <iostream>
#include <set>
#include <cstdio>
#include "config.hpp"

using namespace SuperNN;
using namespace std;

void test_basics()
{
    Network net1 = Network::make_mlp(2, 3, 4);
    
    assert(net1.layers.size() == 3);
    assert(net1.layers[0].size()   == 3); // there's a bias
    assert(net1.layers[1].size()   == 3);
    assert(net1.layers[2].size()   == 4);
    assert(net1.calc_num_weights() == 25);
    
    net1.save_file("temp");
    net1.load_file("temp");
    assert(net1.layers.size()      == 3);
    assert(net1.layers[0].size()   == 3);
    assert(net1.layers[1].size()   == 3);
    assert(net1.layers[2].size()   == 4);
    assert(net1.calc_num_weights() == 25);
    
    Network net2 = Network::make_fcc(5, 2, 2);
    
    assert(net2.layers.size()      == 4);
    assert(net2.layers[0].size()   == 6); // there's a bias
    assert(net2.layers[3].size()   == 2);
    assert(net2.layers[1].size()   == 1);
    assert(net2.calc_num_weights() == 29);
    
    net2.save_file("temp");
    net2.load_file("temp");
    assert(net2.layers.size()      == 4);
    assert(net2.layers[0].size()   == 6);
    assert(net2.layers[3].size()   == 2);
    assert(net2.layers[1].size()   == 1);
    assert(net2.calc_num_weights() == 29);
    
    const double static_data[] = {
        1, 2, 3, 4, 5, 6, 7, 8, 9
    };
    
    Data test_data(3, 3, static_data);
    assert(test_data.size() == 3);
    
    for(unsigned i = 0; i < 3; ++i)
        for(unsigned j = 0; j < 3; ++j)
            assert( test_data[i][j] == i * 3 + j + 1);
            
    test_data.calc_bounds();
    test_data.scale(-1, 1);
    
    for(unsigned i = 0; i < 3; ++i)
        for(unsigned j = 0; j < 3; ++j)
            assert( fabs(test_data[i][j]) <= 1 );
    
    test_data.descale();
    
    for(unsigned i = 0; i < 3; ++i)
        for(unsigned j = 0; j < 3; ++j)
            assert( test_data[i][j] == i * 3 + j + 1);
    
    test_data.save_file("temp");
    test_data.clear();
    test_data.load_file("temp");
    assert(test_data.size() == 3);
    
    for(unsigned i = 0; i < 3; ++i)
        for(unsigned j = 0; j < 3; ++j)
            assert( test_data[i][j] == i * 3 + j + 1);
    
    remove("temp");
}

void test_bounds()
{
    Bounds b1, b2;
    b1.push_back(SInfo(-5, 10));
    b1.push_back(SInfo(5,   7));
    b2.push_back(SInfo(-10, 7));
    b2.push_back(SInfo(10, 20));
    
    b1.merge_with(b2);
    assert(b1[0].min == -10);
    assert(b1[0].max ==  10);
    assert(b1[1].min ==   5);
    assert(b1[1].max ==  20);
}

void test_data()
{
    Data d(1, 1);
    d.from[0] = SInfo(5, 15);
    d.to[0]   = SInfo(0, 1);
    
    Row& r = d.add();
    r[0] = 0.5;
    
    d.descale();
    assert(r[0] == 10);
    
    r[0] = 1;
    d.descale();
    assert(r[0] == 15);
    
    r[0] = 0;
    d.descale();
    assert(r[0] == 5);
    
    std::swap(d.from, d.to);
    d.descale();
    assert(r[0] == 0);
    
    const double bound_data[] = {
        -10, 0, 0,
          0, 1, 2,
          6, 8, 0
    };
    
    Data testb(3, 3, bound_data);
    assert(testb.size() == 3);
    
    testb.calc_bounds();
    testb.scale(-1, 1);
    testb.save_info_file("temp");
    
    Bounds from, to;
    Bounds::load_file("temp", from, to);
    assert(from.size() == 3);
    assert(to.size()   == 3);
    
    for(unsigned i = 0; i < 3; ++i)
    {
        assert(to[i].min == -1);
        assert(to[i].max == 1);
    }
    
    assert(from[0].min == -10); assert(from[0].max == 6);
    assert(from[1].min ==   0); assert(from[1].max == 8);
    assert(from[2].min ==   0); assert(from[2].max == 2);
    
    remove("temp");
    
    {
        Data test(10, 2);
        assert( test.size() == 10 );
        for(unsigned r = 0, e = test.size(); r < e; ++r)
            assert( test[r].size() == 2);
    }

    {
        Row t;
        for(unsigned i = 0; i < 12; ++i)
            t.push_back(i);

        Data test(3, 4, t);
        assert(test.size() == 3);

        for(unsigned r = 0, e = test.size(); r < e; ++r)
        {
            assert(test[r].size() == 4);

            for(unsigned c = 0, e = test[r].size(); c < e; ++c)
            {
                assert(test[r][c] == r*4+c);
            }
        }
    }
    {
        Data test(3, 3);

        for(unsigned r = 0; r < 3; ++r)
            for(unsigned c = 0; c < 3; ++c)
                test.set(r, c, 3*r+c);

        for(unsigned r = 0; r < 3; ++r)
            for(unsigned c = 0; c < 3; ++c)
                assert(test.get(r, c) == 3*r+c);
    }
    
    {
        Data test(13, 10);
        Data o = test.drop_column(9);
        Data o2 = o.drop_column(3);
    
        assert(test.size() == 13);
        assert(o.size()    == 13);
        assert(o2.size()   == 13);
        
        assert(test.from.size() == 10);
        assert(test.to.size()   == 10);
        assert(test.n_total     == 10);
        
        assert(o.from.size()    == 9);
        assert(o.to.size()      == 9);
        assert(o.n_total        == 9);
        
        assert(o2.from.size() == 8);
        assert(o2.to.size()   == 8);
        assert(o2.n_total     == 8);
    }
}

void test_kfold()
{
    Data ktest(100, 1);

    for(unsigned i = 0; i < 100; ++i)
        ktest[i][0] = i;
    
    vector<unsigned> psizes;
    psizes.push_back(2);
    psizes.push_back(5);
    psizes.push_back(10);
    
    for(unsigned i = 0, e = psizes.size(); i < e; ++i)
    {
        unsigned K = psizes[i];
        
        for(unsigned p = 0; p < K; ++p)
        {
            Data part, left;
            ktest.k_fold(p, K, part, left);
            assert(part.size() == 100.0 / K);
            assert(left.size() == 100 - (100.0 / K));
            
            set<double> ts;

            for(unsigned r = 0, e = part.size(); r < e; ++r)
            {
                double c = part[r][0];
                ts.insert(c);
            }
            
            for(unsigned r = 0, e = left.size(); r < e; ++r)
            {
                double c = left[r][0];
                assert(ts.find(c) == ts.end());
                assert(c >= 0 && c < 100);
            }
        }
    }
}

void test_training_running()
{
    Network net1 = Network::make_mlp(3,2,2);
    Network net2 = Network::make_mlp(4,2,2);
    Network net3 = Network::make_mlp(2,2,1);
    
    const double cdata[] = {
        1, 2, 3,
        4, 5, 6,
        7, 8 ,9,
        1, 0, 0
    };
    Data data(4, 3, cdata);
    
    // only net2 should throw an exception when running.
    {
        net1.run(data[1]);
    }
    
    {
        bool exp = false;
    
        try
        {
            net2.run(data[1]);
        } catch(const Exception&)
        {
            exp = true;
        }
        assert(exp);
    }
    
    {
        net3.run(data[1]);
    }
    
    IRprop t;
    
    // net1 and net2 should throw an exception when training.
    {
        bool exp = false;
        
        try
        {
            t.prepare(net1);
            t.train(net1, data, 1, 1);
        }
        catch (const Exception&)
        {
            exp = true;
        }
        assert(exp);
    }
    
    {
        bool exp = false;
        
        try
        {
            t.prepare(net2);
            t.train(net2, data, 1, 1);
        }
        catch (const Exception&)
        {
            exp = true;
        }
        assert(exp);
    }
    
    {
        t.prepare(net3);
        t.train(net3, data, 1, 1);
    }
}

int main()
{
    test_basics();
    test_bounds();
    test_data();
    test_kfold();
    test_training_running();
    
    // the other way around
    test_training_running();
    test_kfold();
    test_data();
    test_bounds();
    test_basics();

    cout << "Everything seems to be alright, after all." << endl;
    
    return 0;
}
