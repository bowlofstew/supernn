#include <supernn>
#include <iostream>
#include "config.hpp"

using namespace SuperNN;
using namespace std;

int main()
{
    Utils::rand_seed();
    
    /* create and configure the topology */
    Network net = Network::make_fcc(4, 1, 3);
    net.set_activation(ACT_SIGMOID_SYMMETRIC);
    net.init_weights(-0.5, 0.5);
    
    /* training algorithm */
    NBN t;
    t.prepare(net);
    
    /* load and scale the data, separating it into two subsets */
    Data full;
    full.load_file(TEST_DIR + "iris/iris.full");
    full.calc_bounds();
    full.scale(-1, 1);
    full.shuffle(); /* small data set, sensitive to the partitions! */
    
    const Data train = full.sample(0  ,  70);
    const Data val   = full.sample(70 , 100);
    const Data test  = full.sample(100, 150);
    
    /* training */
    const unsigned e = early_stopping(t, net, train, val, 1, 10);

    cout << "Epochs: " << e << endl;

    /* save network info (not used in this example) */
    full.save_info_file("iris.info");
    net.save_file("iris.net");
    
    /* testing */
    cout << "\n** Validation data **" << endl;
    cout << "MSE    : " << net.calc_mse(val) << endl;
    cout << "Class  : " << net.calc_class_higher(val) << endl;
    
    cout << "\n** Test data **" << endl;
    cout << "MSE    : " << net.calc_mse(test) << endl;
    cout << "Class  : " << net.calc_class_higher(test) << endl;
    
    return 0;
}
