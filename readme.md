SuperNN
=======

About
-----

SuperNN is an artificial neural network library, with support to arbitrary
feed-forward topologies, that implements state-of-the-art supervised
training  algorithms such as Improved Resilient Backpropagation (iRprop)
[Igel and Hüsken, 2000] and Neuron by Neuron (NBN) [Wilamowski, Yu, 2010].

SuperNN is released under the LGPLv3+ license. It was tested on modern
GNU/Linux distributions and on MS-Windows 8 (by using MingW, VS2010, and VS2013).

Provides an exporter tool that allows one to generate standalone Java or C++
code to use trained neural networks without requiring supernn at runtime.

Dependencies
------------

SuperNN depends on [Eigen][1] (compile time only).

License
-------

LGPLv3+.

Compilation
-----------

    $ mkdir build 
    $ cd build
    $ cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local ..
    $ make -j
    # make install
    
To generate the API documentation to doc/html:

    $ doxygen doc/Doxyfile
    
To include SuperNN into your cmake project you can use the provided module located at
cmake/FindSuperNN.cmake.

You can also generate standalone Java or C++ code to run a trained neural network without requiring
supernn at runtime. The generators are located on the _tools_ folder.

-------

Lucas Hermann Negri - lucashnegri@gmail.com - [OProj][2]

[1]: http://eigen.tuxfamily.org
[2]: http://oproj.tuxfamily.org
